/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //Enable impulse MIDI input
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);
    
    //Add Maincomponent to be a listened to the MIDI input
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    
    //Set text of midi label and make visible
    midiLabel.setText("midilabel", dontSendNotification);
    addAndMakeVisible(midiLabel);
    
    //set default output to be SimpleSynth
    audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
}

MainComponent::~MainComponent()
{
    //Ensure MainComponent stops listening to the MIDI input when the program closes
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    //Set the boundaries and size of the midi Label
    midiLabel.setBounds(0, 0, 300, 20);
}

void MainComponent::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    DBG ("Midi Message Receieved\n");
    
    //make new string called midiText
    String midiText;
    
    //If message is an on or off note then add Note, Number and velocity values to midiText String
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << ": Number " << message.getNoteNumber();
        midiText << ": Velocity " << message.getVelocity();
    }
    //If message is a pitchweel then add pitch wheel values to midiText String
    if (message.isPitchWheel())
    {
        midiText << "PitchWheel value: " << message.getPitchWheelValue();
    }
    //If message is a controller then add Number and value to midiText String
    if (message.isController())
    {
        midiText << "Controller Number: " << message.getControllerNumber();
        midiText << " Controller value: " << message.getControllerValue();
    }
    //Label text becomes the new midiText string
    midiLabel.getTextValue() = midiText;
    
    //forward all incoming messages to SimpleSynth (relates to setDefaultMidiOutput in MainComponent constructor
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (message);
}


//look up initialiser lists
//non blocking thread cues